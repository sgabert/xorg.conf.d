DESTDIR=/etc/X11/xorg.conf.d
CONFIGFILES=$(wildcard *.conf)

RM=rm -f
CP=cp -f

.PHONY: createdir help install uninstall

help:
	@echo "[help] possible targets:"
	@echo " install:   cp all *.conf files into $(DESTDIR)"
	@echo " uninstall: remove all *.conf files from $(DESTDIR)"
	@echo ""

createdir:
	@if [ ! -d "$(DESTDIR)" ]; then \
		echo "[createdir] $(DESTDIR)" ;\
		mkdir -p "$(DESTDIR)";\
	fi

install: createdir
	@echo "[install] copy all config files into $(DESTDIR)"
	for file in $(CONFIGFILES); do \
		$(CP) "$$file" "$(DESTDIR)/." ;\
	done
	@echo "[install] success"

uninstall:
	@echo "[uninstall] remove config files from $(DESTDIR)"
	for file in $(CONFIGFILES); do \
		$(RM) "$(DESTDIR)/$$file" ;\
	done
